//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManiacService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Question
    {
        public short QuestionId { get; set; }
        public string QuestionGroupCode { get; set; }
        public short SequenceNbr { get; set; }
        public bool IsActive { get; set; }
        public string QuestionText { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateBy { get; set; }
    }
}
