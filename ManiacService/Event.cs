//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManiacService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Event
    {
        public int EventId { get; set; }
        public string EventName { get; set; }
        public System.DateTime EventDate { get; set; }
        public string EventURL { get; set; }
        public string EventType { get; set; }
        public string EventLocation { get; set; }
        public string RaceLogoFileName { get; set; }
        public string EventCity { get; set; }
        public Nullable<byte> EventSize { get; set; }
        public Nullable<byte> HasManiacDiscount { get; set; }
        public Nullable<byte> IsCancelled { get; set; }
        public string ManiacDiscountDesc { get; set; }
    }
}
