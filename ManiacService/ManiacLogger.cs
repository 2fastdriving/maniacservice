﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

namespace ManiacService
{
    public class ManiacLogger
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static ManiacLogger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }



        public static void WriteEvent(System.Diagnostics.EventLogEntryType type, string message)
        {

            var url = string.Empty;
            var ua = string.Empty;
            var ip = string.Empty;
            try
            {
                url = HttpContext.Current.Request.RawUrl;
                ua = HttpContext.Current.Request.UserAgent;
                ip = HttpContext.Current.Request.UserHostAddress;
            }
            catch
            {
            }

            message = Environment.NewLine + "IP: " + ip + Environment.NewLine + "Url: " + url + Environment.NewLine + "UserAgent: " + ua + Environment.NewLine + message;

            switch (type)
            {
                case System.Diagnostics.EventLogEntryType.Error:
                    log.Error(message);
                    break;
                case System.Diagnostics.EventLogEntryType.Information:
                    log.Info(message);
                    break;
                case System.Diagnostics.EventLogEntryType.Warning:
                    log.Warn(message);
                    break;
            }
        }

        public static void WriteEvent(Exception ex)
        {
            var url = string.Empty;
            var ua = string.Empty;
            var ip = string.Empty;
            var email = string.Empty;
            try
            {
                url = HttpContext.Current.Request.RawUrl;
                ua = HttpContext.Current.Request.UserAgent;
                ip = HttpContext.Current.Request.UserHostAddress;
            }
            catch
            {
            }

            if (ex != null)
            {
                log.Error(Environment.NewLine + "IP: " + ip + Environment.NewLine + "Url: " + url + Environment.NewLine + "UserAgent: " + ua + Environment.NewLine, ex);
            }

        }


        public static void Debug(string message)
        {
            log.Debug(message);
        }
    }
}