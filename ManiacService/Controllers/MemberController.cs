﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ManiacService.Controllers
{
    public class MemberController : ApiController
    {
        private ManiacEntities db = new ManiacEntities();

        public MemberController()
        {
            
        }

        // GET api/Maniac
        public IHttpActionResult GetMembers()
        {
            if (!IsValid())
            {
                return BadRequest(ModelState);
            }

            return Ok(db.Maniacs);
        }

        // GET api/Maniac/5
        [ResponseType(typeof(Maniac))]
        public IHttpActionResult GetMember(Renewal renewal)
        {
            if (!IsValid())
            {
                return BadRequest(ModelState);
            }

            if (renewal.MemberType.ToLower() == "m")
            {
                Maniac maniac = db.Maniacs.FirstOrDefault(o => o.ManiacId == renewal.Id);
                return Ok(maniac);
            }
            else
            {

                HF hf = db.HFs.FirstOrDefault(o => o.HFId == renewal.Id);
                return Ok(hf);
            }

            return NotFound();
            
        }

        // POST api/Member/renew
        [HttpPost]
        [Route("api/Member/renew")]
        public IHttpActionResult PostRenewal(Renewal renewal)
        {
            ManiacLogger.WriteEvent(System.Diagnostics.EventLogEntryType.Information, "Updating years paid for member " + renewal.Email + ',' + renewal.FirstName + ' ' + renewal.LastName);

            if (!IsValid())
            {
                throw new Exception("Invalid Parameters supplied.");
            }

            if (!MemberExists(renewal))
            {
                throw new Exception("Could not find member record for ID: " + renewal.Id.ToString());
            }

            try
            {
                db.MemberRegUpd(renewal.MemberType, renewal.Email, renewal.FirstName, renewal.LastName, renewal.YearsPaid, renewal.Id);

                MailUtility mail = new MailUtility();
                if (renewal.MemberType.ToLower() == "m")
                {
                    Maniac maniac = db.Maniacs.FirstOrDefault(o => o.ManiacId == renewal.Id);
                    mail.SendMail(EmailTemplateTypes.RenewManiac, maniac);
                }
                else
                {

                    HF hf = db.HFs.FirstOrDefault(o => o.HFId == renewal.Id);
                    mail.SendMail(EmailTemplateTypes.RenewHalfFanatic, hf);

                }
            }
            catch (Exception e)
            {
                try
                {
                    MailUtility mail = new MailUtility();
                    mail.SendErrorMail(e);
                }
                catch { }

                ManiacLogger.WriteEvent(e);
                return InternalServerError(e);
            }

            return Ok();
        }

        private bool IsValid()
        {
            if (!Request.RequestUri.PathAndQuery.Contains("token=" + Properties.Settings.Default.authenticationKey))
            {
                ModelState.AddModelError("error","invalid token");
                return false; ;
            }
            if (!Request.RequestUri.PathAndQuery.Contains("app=" + Properties.Settings.Default.authenticationAuthorizedApplication))
            {
                ModelState.AddModelError("error", "invalid app");
                return false; ;
            }

            return true;
        }

        // POST api/Maniac
        [ResponseType(typeof(NewMember))]
        public IHttpActionResult PostMember(NewMember member)
        {

            try
            {
                ManiacLogger.WriteEvent(System.Diagnostics.EventLogEntryType.Information, "Posting new member.");

                if (!IsValid())
                {
                    throw new Exception("Invalid Parameters supplied.");
                }

                if (MemberExists(member))
                {
                    throw new Exception("Member exists: " + member.Email);
                }

                System.Data.Entity.Core.Objects.ObjectResult<MemberIns_Result> m = db.MemberIns(member.MemberType, member.YearsPaid,
                   member.FirstName, member.LastName, member.Sex, member.Email, member.LevelStarCount, member.State);


                //send email to new maniac with password, depends on type of signup
                //get return value and check for non-zero error count and send that to admin email
                MailUtility mail = new MailUtility();
                if (member.MemberType.ToLower() == "m")
                {
                    Maniac maniac = db.Maniacs.Find(m.FirstOrDefault<MemberIns_Result>().MemberId);

                    if (maniac == null)
                    {
                        throw new Exception("Unknown database failure adding member.");
                    }

                    if (member.YearsPaid == 99)
                    {
                        mail.SendMail(EmailTemplateTypes.WelcomeNewLifetimeManiac, maniac);
                    }
                    else
                    {
                        mail.SendMail(EmailTemplateTypes.WelcomeNewManiac, maniac);
                    }

                    return Ok(maniac);
                }
                else
                {
                    HF hf = db.HFs.Find(m.FirstOrDefault<MemberIns_Result>().MemberId);

                    if (hf == null)
                    {
                        throw new Exception("Unknown database failure adding member.");
                    }

                    if (member.YearsPaid == 99)
                    {
                        mail.SendMail(EmailTemplateTypes.WelcomeNewLifetimeHalfFanatic, hf);
                    }
                    else
                    {
                        mail.SendMail(EmailTemplateTypes.WelcomeNewHalfFanatic, hf);
                    }

                    return Ok(hf);
                }
                
            }
            catch (Exception e)
            {
                try
                {
                    MailUtility mail = new MailUtility();
                    mail.SendErrorMail(e);
                }
                catch { }

                ManiacLogger.WriteEvent(e);
                return InternalServerError(e);
            }
        }

        private bool MemberExists(NewMember m)
        {
            if (m.MemberType.ToLower() == "m")
            {
                return db.Maniacs.Count(e => e.Email == m.Email && e.FirstName == m.FirstName && e.LastName == m.LastName) > 0;
            }

            if (m.MemberType.ToLower() == "h")
            {
                return db.HFs.Count(e => e.Email == m.Email && e.FirstName == m.FirstName && e.LastName == m.LastName) > 0;
            }

            return false;
        }

        private bool MemberExists(Renewal renewal)
        {
            if (renewal.MemberType.ToLower() == "m")
            {
                return db.Maniacs.Count(e => e.ManiacId == renewal.Id) > 0;
            }

            if (renewal.MemberType.ToLower() == "h")
            {
                return db.HFs.Count(e => e.HFId == renewal.Id) > 0;
            }

            return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}