﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManiacService
{
    public enum EmailTemplateTypes
    {
        WelcomeNewManiac = 2,
        WelcomeNewLifetimeManiac = 3,
        WelcomeNewLifetimeHalfFanatic = 4,
        WelcomeNewHalfFanatic = 5,
        RenewManiac = 6,
        RenewHalfFanatic = 7,
        ErrorMessage = 99
    }
}