﻿
/****** Object:  UserDefinedFunction [dbo].[HFLevelName]    Script Date: 2/10/2015 9:58:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[HFLevelName](@LevelNbr int)
returns varchar(10) 
as
begin
   declare @LevelName varchar(10);
   select @LevelName = case @LevelNbr
      when 1 then 'Neptune'
      when 2 then 'Uranus'
      when 3 then 'Saturn'
      when 4 then 'Jupiter'
      when 5 then 'Mars'
      when 6 then 'Earth'
      when 7 then 'Venus'
      when 8 then 'Mercury'
      when 10 then 'The Sun'
      else NULL end;
   
   return @LevelName;
end   

GO



/****** Object:  UserDefinedFunction [dbo].[ManiacLevelName]    Script Date: 2/10/2015 9:59:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[ManiacLevelName](@LevelNbr int)
returns varchar(10) 
as
begin
   declare @LevelName varchar(10);
   select @LevelName = case @LevelNbr
      when 1 then 'Bronze'
      when 2 then 'Silver'
      when 3 then 'Gold'
      when 4 then 'Iridium'
      when 5 then 'Ruthenium'
      when 6 then 'Osmium'
      when 7 then 'Palladium'
      when 8 then 'Platinum'
      when 10 then 'Titanium'
      else NULL end;
   
   return @LevelName;
end   

GO


/****** Object:  UserDefinedFunction [dbo].[StarLevelName]    Script Date: 2/10/2015 9:59:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[StarLevelName](@StarLevelNbr int)
returns varchar(10) 
as
begin
   declare @LevelName varchar(10);
   select @LevelName = case @StarLevelNbr
      when 1 then 'Bronze'
      when 2 then 'Silver'
      when 3 then 'Gold'
      when 4 then 'Iridium'
      when 5 then 'Ruthenium'
      when 6 then 'Osmium'
      when 7 then 'Palladium'
      when 8 then 'Platinum'
      when 10 then 'Titanium'
      else NULL end;
   
   return @LevelName;
end   

GO

