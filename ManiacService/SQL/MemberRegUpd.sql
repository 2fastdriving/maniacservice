﻿USE [maniacdev]
GO

/****** Object:  StoredProcedure [dbo].[MemberRegUpd]    Script Date: 2/10/2015 7:25:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[MemberRegUpd]   
  @MemberType   varchar(1),
  @Email			varchar(100),
  @FirstName        varchar(30),
  @LastName			varchar(30),         
  @YearsPaid		int          = 1
as
	declare @MemberId int

	select @MemberId = 0

	if (@YearsPaid is NULL or @YearsPaid < 1 or (@YearsPaid > 5 and @YearsPaid < 99) or @YearsPaid > 99 )
      raiserror(N'Years Paid is required and must be between 1 and 5 or lifetime (99).', 16, 1);

	if (@MemberType = 'M')
	begin
		select @MemberId = ManiacId from dbo.Maniac where RTRIM(LTRIM(Email)) = @Email and RTRIM(LTRIM(FirstName)) = @FirstName and RTRIM(LTRIM(LastName)) = @LastName

		if (isnull(@MemberId, 0) > 0)
			update dbo.Maniac 
			set PaidThroughDate = dateadd(YEAR, @YearsPaid, PaidThroughDate)
			where  ManiacId = @MemberId
	end

	if (@MemberType = 'H')
	begin
		select @MemberId = HFId from dbo.HF where RTRIM(LTRIM(Email)) = @Email and RTRIM(LTRIM(FirstName)) = @FirstName and RTRIM(LTRIM(LastName)) = @LastName

		if (isnull(@MemberId, 0) > 0)
			update dbo.HF 
			set PaidThroughDate = dateadd(YEAR, @YearsPaid, PaidThroughDate)
			where  HFId = @MemberId
	end

	if (isnull(@MemberId, 0) = 0)
		raiserror(N'No member found for supplied email/name combination', 16, 1);

select @MemberId


GO


