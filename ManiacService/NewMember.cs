//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManiacService
{
    using System;
    
    public partial class NewMember
    {
        public NewMember()
        {
            this.YearsPaid = 1;
        }
    
        public string MemberType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MemberDisplayName { get; set; }
        public string Sex { get; set; }
        public string Email { get; set; }
        public string LoginName { get; set; }
        public string LevelStarCount { get; set; }
        public byte YearsPaid { get; set; }
        public string State { get; set; }
    }
}
