﻿/****** Object:  StoredProcedure [dbo].[MemberIns]    Script Date: 2/10/2015 9:53:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[MemberIns]
  @MemberTypeCode    varchar(1)   = NULL          
 ,@MemberId          int          = NULL          
 ,@YearsPaid         int          = 1
 ,@FirstName         varchar(30)  = NULL
 ,@LastName          varchar(30)  = NULL
 ,@Sex               varchar(1)   = NULL
 ,@Email             varchar(100) = NULL
 ,@LevelStarCount    varchar(2)   = NULL
as
   set nocount on;
   
   declare @ErrCount int
          ,@ErrMsg varchar(1000)
          ,@dtSignupDate datetime
          ,@dtPaidThroughDate datetime
          ,@intLevelNbr int
          ,@ActionData varchar(200)
          ,@CurrentPaidThroughDate datetime
          ,@blIsActiveLogin tinyint
          ,@LoginName varchar(20)
          ,@LevelName varchar(10);
          
   set @ErrCount = 0
   set @ErrMsg = '';
   set @blIsActiveLogin = 1;
   
   if isnull(@FirstName,'') = ''
   or isnull(@LastName,'') = ''
   or isnull(@Sex,'') = ''
   or isnull(@Email,'') = ''
   or isnull(@LevelStarCount,'') = ''
   begin
      select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'First Name, Last Name, Years Paid, Sex, Email and Star Count are required.<br>';
   end;
   
   if isnull(@MemberTypeCode, '') not in ('H','M')
   begin
      select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Member Type is required and must be either H or M.<br>';
   end;
   
   -- If a Maniac/Halffanatic Id is specified, it must not already exist.
   -- Any member set up with a specified login will initially be inactive until manually activated.
   -- This prevents them from showing up in the Insane Asylum until their number comes up in sequence.
   if @MemberId is not null
   begin
      if @MemberId < 1 
      begin
         select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Member Id may not be less than 1. Cannot add new Maniac/Halffanatic.<br>';
      end
      if @MemberTypeCode = 'M' 
      begin
         if exists (select 1 from dbo.Maniac where ManiacId = @MemberId)
         begin
            select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Maniac Id ' + convert(varchar(7), @MemberId) + ' is already in use. Cannot add new Maniac.<br>';
         end;
      end
      else if @MemberTypeCode = 'H'
      begin
         if exists (select 1 from dbo.HF where HFId = @MemberId)
         begin
            select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Halffanatic Id ' + convert(varchar(7), @MemberId) + ' is already in use. Cannot add new Halffanatic.<br>';
         end;
      end;
      set @blIsActiveLogin = 0;
   end;
   
   if @YearsPaid is NULL or @YearsPaid < 1
   begin
      select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Years Paid is required and must be 1 or larger.<br>';
   end
   else
   begin
      select @dtSignupDate = getdate();
      select @dtPaidThroughDate = dateadd(YEAR, @YearsPaid, @dtSignupDate);
   end;

   set @Sex = upper(@Sex);
   if @Sex not in ('F','M','U')
   begin
      select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Sex must be F or M or U.<br>';
   end;

   if isnumeric(@LevelStarCount) = 0 
   begin
      select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'LevelStarCount must be numeric.<br>';
   end
   else
   begin
      set @intLevelNbr = convert(int, @LevelStarCount);
      if @intLevelNbr not in (1,2,3,4,5,6,7,8,10)
      begin
         select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'LevelStarCount must be in (1,2,3,4,5,6,7,8,10).<br>';
      end;
      
   end;

                                                         
   if @ErrCount = 0                                      
   begin
      if @MemberTypeCode = 'M' 
      begin
         select @LevelName = dbo.ManiacLevelName(@intLevelNbr)
         begin tran;
         if @MemberId is null 
         begin
            select @MemberId = dbo.ManiacNextIdGet();
         end;
         select @LoginName = dbo.ManiacNewLoginGet(@MemberId, @FirstName, @LastName);
         if @LoginName is null
         begin
            select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'LoginName ' + @LoginName + ' already exists. Cannot add Maniac.<br>';
         end;

         if @ErrCount = 0
         begin
            insert dbo.Maniac
                  (ManiacId         
                  ,SignupDate       
                  ,PaidThroughDate  
                  ,FirstName        
                  ,LastName         
                  ,ManiacDisplayName
                  ,Sex              
                  ,Email            
                  ,LoginName        
                  ,Password         
                  ,IsActiveLogin    
                  ,LevelStarCount
                  )
            select @MemberId                    ManiacId
                  ,@dtSignupDate                SignupDate          
                  ,@dtPaidThroughDate           PaidThroughDate
                  ,@FirstName                   FirstName
                  ,@LastName                    LastName
                  ,@FirstName + ' ' + @LastName ManiacDisplayName     
                  ,@Sex                         Sex  
                  ,@Email                       Email
                  ,@LoginName                   LoginName
                  ,@LoginName                   Password 
                  ,@blIsActiveLogin             IsActiveLogin
                  ,@intLevelNbr                 LevelStarCount;
            if @@rowcount = 0
            begin
               select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Maniac insert failed for MemberId ' + convert(varchar(6), @MemberId) + '. New Maniac not created.<br>';
            end
            else
            begin
               select @MemberId = ManiacId from dbo.Maniac where LoginName = @LoginName;
               if @MemberId is null 
               begin
                  select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Maniac not found for LoginName ' + @LoginName + '. New Maniac not created.<br>';
               end;
            end;
         end;
         
         if @ErrCount = 0
         begin
            -- Don't worry if ManiacActivityLog record isn't created. It's not essential.
            select @ActionData = 'ManiacId: ' + convert(varchar(6), @MemberId) + ', LoginName: ' + @LoginName + ', Maniac Display Name: ' + @FirstName + ' ' + @LastName + ', PaidThroughDate:' + convert(varchar(19), @dtPaidThroughDate, 121);
            exec dbo.ManiacActivityLogUpd -123, 'MemberIns for ', @ActionData;
            
            insert dbo.ManiacSettings
                  (ManiacId      
                  ,SettingCode   
                  ,SettingValue  
                  ,LastUpdateDate
                  )
            select @MemberId
                  ,'SY'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SL'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SA'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SS'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SC'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'FI'
                  ,'1'
                  ,getdate();
            if @@rowcount = 0
            begin
               select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'ManiacSettings insert failed for MemberId ' + convert(varchar(6), @MemberId) + '. New Maniac not created.<br>'
            end;
         end;
         
         if @ErrCount = 0
         begin
            insert dbo.ManiacYearStats
                  (ManiacId         
                  ,YearNbr          
                  ,LevelStarCount   
                  ,YTDMarathons     
                  ,YTDUltras        
                  ,LifetimeMarathons
                  ,LifetimeUltras   
                  ,AllTimeStreak    
                  ,StateList        
                  ,CountryList      
                  )                 
            select m.ManiacId          
                  ,2000          
                  ,m.LevelStarCount   
                  ,m.YTDMarathons     
                  ,m.YTDUltras        
                  ,m.LifetimeMarathons
                  ,m.LifetimeUltras   
                  ,m.AllTimeStreak    
                  ,m.StateList        
                  ,m.CountryList      
              from dbo.Maniac m
              left join ManiacYearStats mys
                on m.ManiacId = mys.ManiacId
               and mys.YearNbr = 2000
             where m.ManiacId = @MemberId
               and mys.ManiacId is null;
            if @@rowcount = 0
            begin
               select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'ManiaYearStats insert failed for MemberId ' + convert(varchar(6), @MemberId) + '. New Maniac not created.<br>'
            end;
         end;
         
         if @ErrCount = 0
         begin
            commit;
         end
         else
         begin   
            rollback tran;
         end;
      end
      else
      begin
         select @LevelName = dbo.HFLevelName(@intLevelNbr)
         begin tran;
         if @MemberId is null 
         begin
            select @MemberId = dbo.HFNextIdGet();
         end;
         select @LoginName = dbo.HFNewLoginGet(@MemberId, @FirstName, @LastName);
         if @LoginName is null
         begin
            select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'LoginName ' + @LoginName + ' already exists. New Halffanatic not created.<br>';
         end;

--         select 'HF Insert' + ' HFId: ' + convert(varchar(6), isnull(@MemberId, 'NULL')) + ', LoginName: ' + isnull(@LoginName,'NULL');
         if @ErrCount = 0
         begin
            insert dbo.HF
                  (HFId         
                  ,SignupDate       
                  ,PaidThroughDate  
                  ,FirstName        
                  ,LastName         
                  ,HFDisplayName
                  ,Sex              
                  ,Email            
                  ,LoginName        
                  ,Password         
                  ,IsActivated    
                  ,IsEnabled    
                  ,LevelStarCount
                  )
            select @MemberId                    HFId
                  ,@dtSignupDate                SignupDate          
                  ,@dtPaidThroughDate           PaidThroughDate
                  ,@FirstName                   FirstName
                  ,@LastName                    LastName
                  ,@FirstName + ' ' + @LastName HFDisplayName     
                  ,@Sex                         Sex  
                  ,@Email                       Email
                  ,@LoginName                   LoginName
                  ,@LoginName                   Password 
                  ,@blIsActiveLogin             IsActiveLogin
                  ,1                            IsEnabled
                  ,@intLevelNbr                 LevelStarCount;
            if @@rowcount = 0
            begin
               select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'HF insert failed for MemberId ' + convert(varchar(6), @MemberId) + '. New Halffanatic not created.<br>';
            end
            else
            begin
               select @MemberId = HFId from dbo.HF where LoginName = @LoginName;
               if @MemberId is null 
               begin
                  select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'Halffanatic not found for LoginName ' + @LoginName + '. New Halffanatic not created.<br>';
               end;
            end;
         end;
         
         if @ErrCount = 0
         begin
            -- Don't worry if ManiacActivityLog record isn't created. It's not essential.
            select @ActionData = 'HFId: ' + convert(varchar(6), @MemberId) + ', LoginName: ' + @LoginName + ', HF Display Name: ' + @FirstName + ' ' + @LastName + ', PaidThroughDate:' + convert(varchar(19), @dtPaidThroughDate, 121);
            exec dbo.HFActivityLogUpd -1, 'MemberIns for ', @ActionData;
            
            insert dbo.HFSettings
                  (HFId      
                  ,SettingCode   
                  ,SettingValue  
                  ,LastUpdateDate
                  )
            select @MemberId
                  ,'SY'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SL'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SA'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SS'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'SC'
                  ,'N'
                  ,getdate()
            union all                  
            select @MemberId
                  ,'FI'
                  ,'1'
                  ,getdate();
         
            if @@rowcount = 0
            begin
               select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'HFSettings insert failed for MemberId ' + convert(varchar(6), @MemberId) + '. New Halffanatic not created.<br>'
            end;
         end;
         
         if @ErrCount = 0
         begin
            insert dbo.HFYearStats
                  (HFId         
                  ,YearNbr          
                  ,LevelStarCount   
                  ,YTDRaces     
                  ,LifetimeRaces
                  ,AllTimeStreak    
                  ,StateList        
                  ,CountryList      
                  )                 
            select m.HFId          
                  ,2000          
                  ,m.LevelStarCount   
                  ,m.YTDRaces     
                  ,m.LifetimeRaces
                  ,m.AllTimeStreak    
                  ,m.StateList        
                  ,m.CountryList      
              from dbo.HF m
              left join HFYearStats mys
                on m.HFId = mys.HFId
               and mys.YearNbr = 2000
             where m.HFId = @MemberId
               and mys.HFId is null;
            if @@rowcount = 0
            begin
               select @ErrCount = @ErrCount+1, @ErrMsg = @ErrMsg + 'HFYearStats insert failed for MemberId ' + convert(varchar(6), @MemberId) + '. New Halffanatic not created.<br>'
            end;
         end;
         
         if @ErrCount = 0
         begin
            commit;
         end
         else
         begin   
            rollback tran;
         end;
      end;
   end;
   
   -- Returning MemberId implies that a member was added, so if no member was added, return NULL.
   if @ErrCount > 0                                      
   begin
      set @MemberId = NULL;
   end;
   
   -- Return MemberId int, LoginName varchar(20), PaidThroughDate varchar(10), LevelName varchar(10), ErrCount tinyint, ErrMsg varchar(1000)
   select @MemberId MemberId, @LoginName LoginName, CONVERT(varchar(10), @dtPaidThroughDate, 101) PaidThroughDate, @LevelName LevelName, @ErrCount ErrCount, @ErrMsg ErrMsg;

GO


