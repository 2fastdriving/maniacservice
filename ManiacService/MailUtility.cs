﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace ManiacService
{
    public class MailUtility
    {
        public void SendMail(EmailTemplateTypes templateType, Maniac recipient)
        {
            try
            {
                ManiacEntities db = new ManiacEntities();
                int templateId = Convert.ToInt16(templateType);

                EmailTemplate template = FindById(templateId);
                string subject = template.TemplateHeader;
                string body = template.TemplateBody;

                //load subject placeholders
                subject = FillSubject(subject, recipient);

                //load body placeholders
                body = FillBody(body, recipient);

                body = ApplyMasterTemplate((int)template.MasterTemplateId, body);


                MailMessage m = FillMessageInfo(template, subject, body);
                m.To.Add(new MailAddress(recipient.Email));

                SMTPSend(m); 
            }
            catch (Exception ex)
            {
                ManiacLogger.WriteEvent(System.Diagnostics.EventLogEntryType.Error, ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        public void SendMail(EmailTemplateTypes templateType, HF recipient)
        {
            try
            {
                ManiacEntities db = new ManiacEntities();
                int templateId = Convert.ToInt16(templateType);

                EmailTemplate template = FindById(templateId);
                string subject = template.TemplateHeader;
                string body = template.TemplateBody;

                //load subject placeholders
                subject = FillSubject(subject, recipient);

                //load body placeholders
                body = FillBody(body, recipient);
                body = ApplyMasterTemplate((int)template.MasterTemplateId, body);
                
                MailMessage m = FillMessageInfo(template, subject, body);
                m.To.Add(new MailAddress(recipient.Email));

                SMTPSend(m);
            }
            catch (Exception ex)
            {
                ManiacLogger.WriteEvent(System.Diagnostics.EventLogEntryType.Error, ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        private static MailMessage FillMessageInfo(EmailTemplate template, string subject, string body)
        {
            MailMessage m = new MailMessage();
            m.IsBodyHtml = true;
            m.Sender = new MailAddress(template.EmailFrom);
            m.From = new MailAddress(template.EmailFrom);
            m.Subject = subject;
            m.Body = body;

#if !DEBUG
                string[] cc = template.Emailcc.Replace(',', ';').Split(';');
                for (int i = 0; i < cc.Length; i++)
                {
                    if (cc[i].Length > 0)
                    {
                        m.CC.Add(cc[i]);
                    }

                }
#endif

            return m;
        }

        private static void SMTPSend(MailMessage m)
        {
            var client = new SmtpClient();
            client.Host = Properties.Settings.Default.SmtpServer;
            client.Port = Convert.ToInt16(Properties.Settings.Default.SmtpPort);
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.EnableSsl = false;
            client.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.SmtpUserName, Properties.Settings.Default.SmtpPassword);
            ManiacLogger.WriteEvent(System.Diagnostics.EventLogEntryType.Information, "Sending email from" + m.From.Address + " to " + m.To.First().Address);
            client.Send(m);
        }

        public void SendErrorMail(Exception exception)
        {
            try
            {
                ManiacEntities db = new ManiacEntities();
                int templateId = Convert.ToInt16(EmailTemplateTypes.ErrorMessage);

                EmailTemplate template = FindById(templateId);
                string subject = template.TemplateHeader;
                string body = template.TemplateBody;

                //load subjectg placeholders
                subject = FillSubject(subject, exception);

                //load body placeholders
                body = FillBody(body, exception);

                body = ApplyMasterTemplate((int)template.MasterTemplateId, body);

                MailMessage m = FillMessageInfo(template, subject, body);
                m.To.Add(new MailAddress(Properties.Settings.Default.adminEmail));

                SMTPSend(m);
            }
            catch (Exception ex)
            {
                ManiacLogger.WriteEvent(System.Diagnostics.EventLogEntryType.Error, ex.Message + " " + ex.StackTrace);
                throw;
            }
        }

        private string FillBody(string body, Exception exception)
        {
            body = body.Replace("{errorMessage}", exception.Message + " " + exception.StackTrace);

            return body;
        }

        private string FillSubject(string subject, Exception exception)
        {
            return subject;
        }

        private EmailTemplate FindById(int templateId)
        {
            ManiacEntities db = new ManiacEntities();
            var template = db.EmailTemplates.SingleOrDefault(s => s.EmailTemplateId == templateId);
            return template;
        }

        public string ApplyMasterTemplate(int masterTemplateId, string body)
        {
            EmailTemplate template = FindById(masterTemplateId);

            string masterTemplate = template.TemplateBody;
            //masterTemplate = masterTemplate.Replace("{host}", Properties.Settings.Default.Host);
            masterTemplate = masterTemplate.Replace("{bodyPlaceHolder}", body);

            return masterTemplate;
        }

        private string FillBody(string body, HF recipient)
        {
            body = body.Replace("{memberFirstName}", recipient.FirstName);
            body = body.Replace("{memberLastName}", recipient.LastName);
            body = body.Replace("{memberEmail}", recipient.Email);
            body = body.Replace("{memberLogin}", recipient.LoginName);
            body = body.Replace("{memberId}", recipient.HFId.ToString());
            body = body.Replace("{memberPaidThroughDate}", recipient.PaidThroughDate.ToShortDateString());

            return body;
        }

        private string FillBody(string body, Maniac recipient)
        {
            body = body.Replace("{memberFirstName}", recipient.FirstName);
            body = body.Replace("{memberLastName}", recipient.LastName);
            body = body.Replace("{memberEmail}", recipient.Email);
            body = body.Replace("{memberLogin}", recipient.LoginName);
            body = body.Replace("{memberId}", recipient.ManiacId.ToString());
            body = body.Replace("{memberPaidThroughDate}", recipient.PaidThroughDate.ToShortDateString());

            return body;
        }

        private string FillSubject(string subject, HF recipient)
        {
            ManiacEntities db = new ManiacEntities();
            string levelName = db.HFLevelName(recipient.LevelStarCount).First();
            subject = subject.Replace("{levelName}", levelName);
            subject = subject.Replace("{memberId}", recipient.HFId.ToString());

            return subject;
        }

        private string FillSubject(string subject, Maniac recipient)
        {
            ManiacEntities db = new ManiacEntities();
            string levelName = db.ManiacLevelName(recipient.LevelStarCount).First();
            subject = subject.Replace("{levelName}", levelName);
            subject = subject.Replace("{memberId}", recipient.ManiacId.ToString());

            return subject;
        }

        private string FillBody(string body)
        {
           // body = body.Replace("{memberPostUrl}", Properties.Settings.Default.MemberPostVenue);
            

            return body;
        }

    }
}