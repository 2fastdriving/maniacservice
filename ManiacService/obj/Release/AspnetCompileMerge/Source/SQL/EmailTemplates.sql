﻿USE [maniacdev]
GO

/****** Object:  Table [dbo].[EmailTemplates]    Script Date: 2/3/2015 8:59:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmailTemplates](
	[EmailTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[TemplateCode] [varchar](50) NULL,
	[TemplateHeader] [varchar](200) NOT NULL,
	[TemplateBody] [nvarchar](max) NOT NULL,
	[MasterTemplateId] [int] NULL,
	[Notes] [varchar](500) NULL,
	[EmailFrom] [varchar](100) NULL,
	[Emailcc] [varchar](255) NULL,
	[EmailBcc] [varchar](255) NULL,
	[EmailReplyTo] [varchar](255) NULL,
 CONSTRAINT [PK_EmailTemplates] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_EmailTemplates_Code] UNIQUE NONCLUSTERED 
(
	[TemplateCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



delete emailtemplates
go
SET IDENTITY_INSERT EmailTemplates ON 

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (100
           ,'ManiacTemplate'
		   ,''
           ,'<html><body style="font-family:arial;font-size:10pt">  <div style="margin:0px;padding:0px;">{bodyPlaceHolder}</div><p>
        <p>Steve/Chris/Tony/Marc/Brian</p>
    </p>
    <p><b><span style="font-family:arial;font-size:10pt;color:gray;">Marathon Maniacs</span></b>
    <span style="font-family:arial;font-size:10pt;color:gray;"><br>3812 N 26th St., Ste C<br>Tacoma, WA 98407<br></span>
    <a href="http://www.marathonmaniacs.com" target="_blank">http://www.marathonmaniacs.com</a></p>

    <p><br><a href="https://www.facebook.com/pages/Marathon-Maniacs/144969288167" target="_blank"><span style="color:windowtext;text-decoration:none;">
    <img border="0" width="32" height="32" src="http://marathonmaniacs.com/images/facebook-icon%201.png"></span></a>
    <a href="http://instagram.com/marathonmaniacs" target="_blank">
    <span style="color:windowtext;text-decoration:none;"><img border="0" width="32" height="32" src="http://marathonmaniacs.com/images/InstagramLarge.png"></span></a>&nbsp;
    <a href="https://twitter.com/mainmaniacs" target="_blank"><span style="color:windowtext;text-decoration:none;">
    <img border="0" width="32" height="32" src="http://marathonmaniacs.com/images/Twitter-T.png"></span></a>&nbsp;
    <a href="http://visitor.constantcontact.com/manage/optin/ea?v=001sWE8kxl6JonX7OSB8Oto-HyQpmQ7rE9zZ6Ez2QUiQYGMBOtKxRD9v-OcwUdCZ1DgPYebo4JUHEU%3D" target="_blank">
        <span style="color:windowtext;text-decoration:none;"><img border="0" width="32" height="32" src="http://static.ctctcdn.com/lp/images/standard/icons/icon_email_marketing.png"></span></a></p></body></html>'
        ,NULL
           ,'Maniacs template with header and footer'
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (101
           ,'FanaticsTemplate'
		   ,''
           ,'<html><body style="font-family:arial;font-size:10pt">  <div style="margin:0px;padding:0px;">{bodyPlaceHolder}</div><p>
        <p>Steve/Chris/Tony/Marc/Brian</p>
    </p>
    <p><b><span style="font-family:arial;font-size:10pt;color:gray;">Half Fanatics</span></b>
    <span style="font-family:arial;font-size:10pt;color:gray;"><br>3812 N 26th St., Ste C<br>Tacoma, WA 98407<br></span>
    <a href="http://www.halffanatics.com" target="_blank">http://www.halffanatics.com</a></p>

    <p><br><a href="https://www.facebook.com/pages/Half-Fanatics/151528040988" target="_blank"><span style="color:windowtext;text-decoration:none;">
    <img border="0" width="32" height="32" src="http://halffanatics.com/images/facebook-icon%201.png"></span></a>
    <a href="http://instagram.com/halffanatics" target="_blank">
        <span style="color:windowtext;text-decoration:none;"><img border="0" width="32" height="32" src="http://halffanatics.com/images/InstagramLarge.png"></span></a>&nbsp;
    <a href="https://twitter.com/MainFanatics" target="_blank"><span style="color:windowtext;text-decoration:none;">
    <img border="0" width="32" height="32" src="http://halffanatics.com/images/Twitter-T.png"></span></a>&nbsp;
    <a href="http://visitor.constantcontact.com/manage/optin/ea?v=001sWE8kxl6JonX7OSB8Oto-HyQpmQ7rE9zZ6Ez2QUiQYGMBOtKxRD9v-OcwUdCZ1DgPYebo4JUHEU%3D" target="_blank">
        <span style="color:windowtext;text-decoration:none;"><img border="0" width="32" height="32" src="http://static.ctctcdn.com/lp/images/standard/icons/icon_email_marketing.png"></span></a></p></body></html>'
           ,NULL
           ,'Fanatics template with header and footer'
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (102
           ,'ErrorTemplate'
		   ,''
           ,'<html><body style="font-family:arial;font-size:10pt">{bodyPlaceHolder}</body></html>'
        ,NULL
           ,'Error template'
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (2
           ,'WelcomeNewManiac'
		   ,'Maniac #{memberId}'
           ,'{memberFirstName}, at last you have found refuge, a place where you can call home, where the Maniacal can feel Normal again, and once again be treated like a normal human being.<br/><br/>
Welcome To The Marathon Maniacs AsyLum!!!!<br/><br/>If you go to the Marathon Maniacs Asylum link on the front page, you will see the Member Login link.  Your UserID and Password are (they are the same):<br/><br/>
{memberLogin}<br/><br/>We recommend that you change your UserID and/or Password but you my keep the original values if you wish.  We don''t require you to change them.<br/><br/>
Once you get logged in you can do the following:<br/><br/>- Initialize your statistics by choosing the My Statistics link. This includes your "streak text" , your Year To Date and Lifetime numbers and your states/countries. While this isn''t necessary, 
it is fun item for other members to see. Note that you enter your race history (see below) you can choose to have your stars, numbers, states and countries automatically calculated from your race history.<br/><br/>
- Create a list of your marathon history by selecting the Add My Race link. Once you have entered at least one race, a link will be created on your name when your name appears in the Race Calendar and Marathon Maniacs Asylum. The link will take the user to your race history (your My Marathons page).
<br/><br/>- Change how your statistics are calculated by using the My Preferences link.  Your initial settings are for Manual updating.  You can choose to Automatically update some or all of your statistics from your race history but you need to have all your races in your My Marathons section before 
doing so since the Automatic calculation is performed from that list.<br/><br/>- Edit your Personal Information by choosing the My Personal Info link.<br/><br/>- Add your name to any race listed in our Race Calendar. If you don''t see a race listed, click on the link at the top of the calendar to send us the information about the race.
<br/><br/>- Use the Bulletin Board for members only to see Discounts and discuss training, races, or whatever.
<br/><br/>- You can fill out a Profile that is available to other members by clicking on the My Profile link (you need to have at least one race listed in your My Races section for your profile to be available to other members).
<br/><br/>- To print out a Maniac Certificate go to:
<br/><br/><a href="http://marathonmaniacs.com/MM_Cert.pdf">http://marathonmaniacs.com/MM_Cert.pdf</a>
<br/><br/>- If you would like to be included in the next Maniac newsletter, please send an email with a photo to Steve Walters MM #388 (26freak@gmail.com).  And, if you have future photos or race reports, please send those to Steve as well.
<br/><br/>Your membership is paid through {memberPaidThroughDate}.<br/><br/>
Please take a moment and join our once a month eFlash!  You don''t want to miss out on great discounts and valuable club information.  Your email will not be given out to anyone and you can opt out anytime you wish.  Join eFlash by clicking the envelope icon below, and while you''re at it, check out our 
social media pages as well!<br/><br/>
Be sure to visit the Marathon Maniacs store to get your Gear!!! Click a product below to get started. <br/>
<a href="http://www.databarevents.com/store/category/1/Marathon-Maniacs">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/a5c65-mm_nightlife_coats.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/d8178-pink_nightlife.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/ca123-yellowhoodie.jpg" border="0" width="100" height="124">
</a><br/><br/>
Thanks and Welcome to the Maniac AsyLum!!!!'
           ,100
           ,NULL
           ,'noreply@marathonmaniacs.com'
           ,'kc7wpd@hotmail.com; chris@marathonmaniacs.com; marc@marathonmaniacs.com; steve@marathonmaniacs.com; tony@marathonmaniacs.com'
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (3
           ,'WelcomeNewLifetimeManiac'
		   ,'LifeTime Maniac #{memberId}'
           ,'{memberFirstName}, at last you have found refuge, a place where you can call home, where the Maniacal can feel Normal again, and once again be treated like a normal human being.<br/><br/>
Welcome To The Marathon Maniacs AsyLum for the rest of your life!!!!<br/><br/>If you go to the Marathon Maniacs Asylum link on the front page, you will see the Member Login link.  Your UserID and Password are (they are the same):<br/><br/>
{memberLogin}<br/><br/>We recommend that you change your UserID and/or Password but you my keep the original values if you wish.  We don''t require you to change them.<br/><br/>
Once you get logged in you can do the following:<br/><br/>- Initialize your statistics by choosing the My Statistics link. This includes your "streak text" , your Year To Date and Lifetime numbers and your states/countries. While this isn''t necessary, 
it is fun item for other members to see. Note that you enter your race history (see below) you can choose to have your stars, numbers, states and countries automatically calculated from your race history.<br/><br/>
- Create a list of your marathon history by selecting the Add My Race link. Once you have entered at least one race, a link will be created on your name when your name appears in the Race Calendar and Marathon Maniacs Asylum. The link will take the user to your race history (your My Marathons page).
<br/><br/>- Change how your statistics are calculated by using the My Preferences link.  Your initial settings are for Manual updating.  You can choose to Automatically update some or all of your statistics from your race history but you need to have all your races in your My Marathons section before 
doing so since the Automatic calculation is performed from that list.<br/><br/>- Edit your Personal Information by choosing the My Personal Info link.<br/><br/>- Add your name to any race listed in our Race Calendar. If you don''t see a race listed, click on the link at the top of the calendar to send us the information about the race.
<br/><br/>- Use the Bulletin Board for members only to see Discounts and discuss training, races, or whatever.
<br/><br/>- You can fill out a Profile that is available to other members by clicking on the My Profile link (you need to have at least one race listed in your My Races section for your profile to be available to other members).
<br/><br/>- To print out a Maniac Certificate go to:
<br/><br/><a href="http://marathonmaniacs.com/MM_Cert.pdf">http://marathonmaniacs.com/MM_Cert.pdf</a>
<br/><br/>- If you would like to be included in the next Maniac newsletter, please send an email with a photo to Steve Walters MM #388 (26freak@gmail.com).  And, if you have future photos or race reports, please send those to Steve as well.
<br/><br/>Please take a moment and join our once a month eFlash!  You don''t want to miss out on great discounts and valuable club information.  Your email will not be given out to anyone and you can opt out anytime you wish.  Join eFlash by clicking the envelope icon below, and while you''re at it, check out our 
social media pages as well!<br/><br/>
Be sure to visit the Marathon Maniacs store to get your Gear!!! Click a product below to get started. <br/><br/>
<a href="http://www.databarevents.com/store/category/1/Marathon-Maniacs">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/a5c65-mm_nightlife_coats.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/d8178-pink_nightlife.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/ca123-yellowhoodie.jpg" border="0" width="100" height="124">
</a><br/><br/>
Thanks and Welcome to the Maniac AsyLum!!!!'
           ,100
           ,NULL
           ,'noreply@marathonmaniacs.com'
           ,'kc7wpd@hotmail.com; chris@marathonmaniacs.com; marc@marathonmaniacs.com; steve@marathonmaniacs.com; tony@marathonmaniacs.com'
           ,NULL
           ,NULL)
GO


INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (4
           ,'WelcomeNewLifetimeHalfFanatic'
		   ,'LifeTime Half Fanatic #{memberId}'
           ,'{memberFirstName}, at last you have found refuge, a place where you can call home, where the Fanatical can feel Normal again, and once again be treated like a normal human being.<br/><br/>
Welcome To The Fanatics AsyLum for the rest of your life!!!!<br/><br/>If you go to the Fanatics Asylum link on the front page, you will see the Member Login link.  Your UserID and Password are (they are the same):<br/><br/>
{memberLogin}<br/><br/>We recommend that you change your UserID and/or Password but you my keep the original values if you wish.  We don''t require you to change them.<br/><br/>
Once you get logged in you can do the following:<br/><br/>- Initialize your statistics by choosing the My Statistics link. This includes your "streak text" , your Year To Date and Lifetime numbers and your states/countries. While this isn''t necessary, 
it is fun item for other members to see. Note that you enter your race history (see below) you can choose to have your stars, numbers, states and countries automatically calculated from your race history.<br/><br/>
- Create a list of your half marathon history by selecting the Add My Race link. Once you have entered at least one race, a link will be created on your name when your name appears in the Race Calendar and Fanatic Asylum. The link will take the user to your race history (your My Races page).
<br/><br/>- Change how your statistics are calculated by using the My Preferences link.  Your initial settings are for Manual updating.  You can choose to Automatically update some or all of your statistics from your race history but you need to have all your races in your My Races section before 
doing so since the Automatic calculation is performed from that list.<br/><br/>- Edit your Personal Information by choosing the My Personal Info link.<br/><br/>- Add your name to any race listed in our Race Calendar. If you don''t see a race listed, click on the link at the top of the calendar to send us the information about the race.
<br/><br/>- Use the Bulletin Board for members only to see Discounts and discuss training, races, or whatever.
<br/><br/>- You can fill out a Profile that is available to other members by clicking on the My Profile link (you need to have at least one race listed in your My Races section for your profile to be available to other members).
<br/><br/>- If you would like to be included in the next Fanatic newsletter, please send an email with a photo to Danene Spaeth HF #5005 (halffanaticnewsletter@gmail.com).  And, if you have future photos or race reports, please send those to Danene as well.
<br/><br/>Please take a moment and join our once a month eFlash!  You don''t want to miss out on great discounts and valuable club information.  Your email will not be given out to anyone and you can opt out anytime you wish.  Join eFlash by clicking the envelope icon below, and while you''re at it, check out our 
social media pages as well!<br/><br/>
Be sure to visit the Half Fanatics store to get your Gear!!! Click a product below to get started. <br/><br/>
<a href="http://www.databarevents.com/store/category/2/Half-Fanatics">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/db5e5-hf_coat.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/78b9a-hf_nightlifepink.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/e091e-hf_cotton.jpg" border="0" width="100" height="124">
</a><br/><br/>
Thanks and Welcome to the Fanatic AsyLum!!!!'
           ,101
           ,NULL
           ,'noreply@marathonmaniacs.com'
           ,'kc7wpd@hotmail.com; chris@marathonmaniacs.com; marc@marathonmaniacs.com; steve@marathonmaniacs.com; tony@marathonmaniacs.com'
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (5
           ,'WelcomeNewHalfFanatic'
		   ,'Half Fanatic #{memberId}'
           ,'{memberFirstName}, at last you have found refuge, a place where you can call home, where the Fanatical can feel Normal again, and once again be treated like a normal human being.<br/><br/>
Welcome To The Fanatics AsyLum!!!!<br/><br/>If you go to the Fanatics Asylum link on the front page, you will see the Member Login link.  Your UserID and Password are (they are the same):<br/><br/>
{memberLogin}<br/><br/>We recommend that you change your UserID and/or Password but you my keep the original values if you wish.  We don''t require you to change them.<br/><br/>
Once you get logged in you can do the following:<br/><br/>- Initialize your statistics by choosing the My Statistics link. This includes your "streak text" , your Year To Date and Lifetime numbers and your states/countries. While this isn''t necessary, 
it is fun item for other members to see. Note that you enter your race history (see below) you can choose to have your stars, numbers, states and countries automatically calculated from your race history.<br/><br/>
- Create a list of your half marathon history by selecting the Add My Race link. Once you have entered at least one race, a link will be created on your name when your name appears in the Race Calendar and Fanatic Asylum. The link will take the user to your race history (your My Races page).
<br/><br/>- Change how your statistics are calculated by using the My Preferences link.  Your initial settings are for Manual updating.  You can choose to Automatically update some or all of your statistics from your race history but you need to have all your races in your My Races section before 
doing so since the Automatic calculation is performed from that list.<br/><br/>- Edit your Personal Information by choosing the My Personal Info link.<br/><br/>- Add your name to any race listed in our Race Calendar. If you don''t see a race listed, click on the link at the top of the calendar to send us the information about the race.
<br/><br/>- Use the Bulletin Board for members only to see Discounts and discuss training, races, or whatever.
<br/><br/>- You can fill out a Profile that is available to other members by clicking on the My Profile link (you need to have at least one race listed in your My Races section for your profile to be available to other members).
<br/><br/>- If you would like to be included in the next Fanatic newsletter, please send an email with a photo to Danene Spaeth HF #5005 (halffanaticnewsletter@gmail.com).  And, if you have future photos or race reports, please send those to Danene as well.
<br/><br/>Please take a moment and join our once a month eFlash!  You don''t want to miss out on great discounts and valuable club information.  Your email will not be given out to anyone and you can opt out anytime you wish.  Join eFlash by clicking the envelope icon below, and while you''re at it, check out our 
social media pages as well!<br/><br/>
Be sure to visit the Half Fanatics store to get your Gear!!! Click a product below to get started. <br/><br/>
<a href="http://www.databarevents.com/store/category/2/Half-Fanatics">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/db5e5-hf_coat.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/78b9a-hf_nightlifepink.jpg" border="0" width="100" height="124">
<img src="http://www.databarevents.com/assets/uploads/store_product_images/e091e-hf_cotton.jpg" border="0" width="100" height="124">
</a><br/><br/>
Thanks and Welcome to the Fanatic AsyLum!!!!'
           ,101
           ,NULL
           ,'noreply@marathonmaniacs.com'
           ,'kc7wpd@hotmail.com; chris@marathonmaniacs.com; marc@marathonmaniacs.com; steve@marathonmaniacs.com; tony@marathonmaniacs.com'
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (6
           ,'RenewManiac'
		   ,'Maniac #{memberId}'
           ,'{memberFirstName}, thank you for your renewal!  You are now paid through {memberPaidThroughDate}.'
           ,100
           ,NULL
           ,'noreply@marathonmaniacs.com'
           ,'kc7wpd@hotmail.com; chris@marathonmaniacs.com; marc@marathonmaniacs.com; steve@marathonmaniacs.com; tony@marathonmaniacs.com'
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (7
           ,'RenewHalfFanatic'
		   ,'Half Fanatic #{memberId}'
           ,'{memberFirstName}, thank you for your renewal!  You are now paid through {memberPaidThroughDate}.'
           ,101
           ,NULL
           ,'noreply@marathonmaniacs.com'
           ,'kc7wpd@hotmail.com; chris@marathonmaniacs.com; marc@marathonmaniacs.com; steve@marathonmaniacs.com; tony@marathonmaniacs.com'
           ,NULL
           ,NULL)
GO


INSERT INTO [dbo].[EmailTemplates]
           (EmailTemplateId
		   ,[TemplateCode]
           ,[TemplateHeader]
           ,[TemplateBody]
           ,[MasterTemplateId]
           ,[Notes]
           ,[EmailFrom]
           ,[Emailcc]
           ,[EmailBcc]
           ,[EmailReplyTo])
     VALUES
           (99
           ,'ErrorMessage'
		   ,'Error occurred'
           ,'{errorMessage}'
           ,102
           ,NULL
           ,'noreply@marathonmaniacs.com'
           ,'rossimr@hotmail.com'
           ,NULL
           ,NULL)
GO